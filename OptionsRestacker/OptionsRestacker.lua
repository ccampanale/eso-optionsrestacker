------------------------------------------------------------------
--OptionsRestacker.lua
--Author: Vicster0
--v0.1.5
--[[
	Restacks the options menu based on user preference.
]]
------------------------------------------------------------------
local LAM = LibStub("LibAddonMenu-1.0");
local LAM_optionsRestacker = _optionsRestacker;

OptionsRestacker = {}
OptionsRestacker.Name = "OptionsRestacker"
OptionsRestacker.Version = "0.1.5"
OptionsRestacker.settings = nil
OptionsRestacker.OptionPanels = {}
OptionsRestacker.ParentMenuControl = nil
OptionsRestacker.defaultAlertType = UI_ALERT_CATEGORY_ALERT
OptionsRestacker.defaultAlertSound = nil
OptionsRestacker.lastPanelControl = nil
OptionsRestacker.SI_GAME_MENU_SETTINGS = GetString(SI_GAME_MENU_SETTINGS)
OptionsRestacker.SI_GAME_MENU_CONTROLS = GetString(SI_GAME_MENU_CONTROLS)
OptionsRestacker.SI_MAIN_MENU_SOCIAL = GetString(SI_MAIN_MENU_SOCIAL)
OptionsRestacker.SI_VIDEO_OPTIONS_TITLE = GetString(SI_VIDEO_OPTIONS_TITLE)
OptionsRestacker.SI_AUDIO_OPTIONS_TITLE = GetString(SI_AUDIO_OPTIONS_TITLE)
OptionsRestacker.SI_GAMEPLAY_OPTIONS_TITLE = GetString(SI_GAMEPLAY_OPTIONS_TITLE)
OptionsRestacker.SI_INTERFACE_OPTIONS_TITLE = GetString(SI_INTERFACE_OPTIONS_TITLE)
local OptionsRestacker = OptionsRestacker

function OptionsRestacker.OR_DelayedGameMenu(control, oldState, newState)
	if(newState == "showing") then
		zo_callLater(OptionsRestacker.OR_SetNewOptionsPanelOrder, 500)
	elseif(newState == "hiding") then
		--nothing.. yet
	end
end

function OptionsRestacker.OR_SetNewOptionsPanelOrder()
	for headerName, headerValue in pairs(OptionsRestacker.settings.restackMenuHeaders) do
		local headerContainer, childIndent, childSpacing = OptionsRestacker.OR_GetInGameMenuHeaderContainer( headerName )
		if(headerContainer)then
			OptionsRestacker.OR_DropmenuRefreshItems()
			OptionsRestacker.OR_CollectPanelChildrenFromContainer( headerContainer )
			if(OptionsRestacker.settings.restackAlphabetically and not headerValue)then
				OptionsRestacker.OR_RestackOptionsUserPanels( childIndent, childSpacing, headerName )
			elseif(OptionsRestacker.settings.restackCustomOrder[headerName])then
				OptionsRestacker.OR_RestackOptionsUserPanelsCustom( childIndent, headerName, headerContainer, childSpacing )		
			end
		else
			OptionsRestacker.OR_RemoveMenuHeaderContainerFromSettings( headerName )
		end
	end
end

function OptionsRestacker.OR_RestackOptionsUserPanels( childIndent, childSpacing, headerName )

	local function spairs(t, order)
	    -- collect the keys
	    local keys = {}
	    for k in pairs(t) do keys[#keys+1] = k end

	    -- if order function given, sort by it by passing the table and keys a, b,
	    -- otherwise just sort the keys 
	    if order then
	        table.sort(keys, function(a,b) return order(t, a, b) end)
	    else
	        table.sort(keys)
	    end

	    -- return the iterator function
	    local i = 0
	    return function()
	        i = i + 1
	        if keys[i] then
	            return keys[i], t[keys[i]]
	        end
	    end
	end

	--.parentNode:GetChildSpacing()

	if(OptionsRestacker.OptionPanels and OptionsRestacker.ParentMenuControl) then
		local previousNodeName = nil
		if(OptionsRestacker.settings.ignoreDefaultSettingsPanels and headerName == GetString(SI_GAME_MENU_SETTINGS))then
			previousNodeName = OptionsRestacker.SI_MAIN_MENU_SOCIAL
		end
		for panelName, panelControl in spairs(OptionsRestacker.OptionPanels) do
			--check for default panels and ignore
			if(OptionsRestacker.settings.ignoreDefaultSettingsPanels and (panelName == OptionsRestacker.SI_VIDEO_OPTIONS_TITLE or panelName == OptionsRestacker.SI_AUDIO_OPTIONS_TITLE or panelName == OptionsRestacker.SI_GAMEPLAY_OPTIONS_TITLE or panelName == OptionsRestacker.SI_INTERFACE_OPTIONS_TITLE or panelName == OptionsRestacker.SI_MAIN_MENU_SOCIAL) and headerName == OptionsRestacker.SI_GAME_MENU_SETTINGS) then
				--do nothing...
			else
				--loop through controls in controls
				if(not previousNodeName) then
					panelControl:ClearAnchors()
				    panelControl:SetAnchor(TOPLEFT, OptionsRestacker.ParentMenuControl, TOPLEFT, childIndent, 0)
				else      
					local previousNode  = OptionsRestacker.OptionPanels[previousNodeName]
					panelControl:ClearAnchors()
				    panelControl:SetAnchor(TOPLEFT, previousNode, BOTTOMLEFT, 0, childSpacing)
				end
				--process arbitrary settings
				if(OptionsRestacker.settings.removeColorFromSettings) then
					panelControl:SetText(panelControl:GetText():gsub("|c......",""):gsub("|r",""))
				end
				---------------------------
				previousNodeName = panelName
			end
		end
	end
end

function OptionsRestacker.OR_RestackOptionsUserPanelsCustom( childIndent, menuName, headerContainer, childSpacing )

	--.parentNode:GetChildSpacing()

	if(OptionsRestacker.OptionPanels and OptionsRestacker.ParentMenuControl and OptionsRestacker.settings.restackCustomOrder[menuName]) then
		local previousNodeName = nil
		if(OptionsRestacker.settings.ignoreDefaultSettingsPanels and headerName == OptionsRestacker.SI_GAME_MENU_SETTINGS)then
			previousNodeName = OptionsRestacker.SI_MAIN_MENU_SOCIAL
		end
		for index, customItem in pairs(OptionsRestacker.settings.restackCustomOrder[menuName]) do
			--check for default panels and ignore
			if(OptionsRestacker.settings.ignoreDefaultSettingsPanels and (customItem.settingName == OptionsRestacker.SI_VIDEO_OPTIONS_TITLE or customItem.settingName == OptionsRestacker.SI_AUDIO_OPTIONS_TITLE or customItem.settingName == OptionsRestacker.SI_GAMEPLAY_OPTIONS_TITLE or customItem.settingName == OptionsRestacker.SI_INTERFACE_OPTIONS_TITLE or customItem.settingName == OptionsRestacker.SI_MAIN_MENU_SOCIAL) and headerName == OptionsRestacker.SI_GAME_MENU_SETTINGS) then
				--do nothing...
			else
				--loop through controls in controls
				local targetPanel = OptionsRestacker.OptionPanels[customItem.settingName]
				if(targetPanel) then
					if(customItem.isActiveSetting) then
						if(not previousNodeName) then
							targetPanel:ClearAnchors()
						    targetPanel:SetAnchor(TOPLEFT, OptionsRestacker.ParentMenuControl, TOPLEFT, childIndent, 0)
						else      
							local previousNode  = OptionsRestacker.OptionPanels[previousNodeName]
							targetPanel:ClearAnchors()
						    targetPanel:SetAnchor(TOPLEFT, previousNode, BOTTOMLEFT, 0, childSpacing)
						end
						--process arbitrary settings
						if(OptionsRestacker.settings.removeColorFromSettings) then
							targetPanel:SetText(targetPanel:GetText():gsub("|c......",""):gsub("|r",""))
						end				
						previousNodeName = customItem.settingName
					elseif(customItem.settingName ~= "Options Restacker") then
						targetPanel:ClearAnchors()
						targetPanel:SetHidden(true)
						targetPanel:SetText("")
						targetPanel:SetHeight(0 - childSpacing)
					end
				else
					-- clean up old entry from custom db
					table.remove(OptionsRestacker.settings.restackCustomOrder, index)
				end
			end
		end
	end
end

function OptionsRestacker.OR_CollectPanelChildrenFromContainer( headerContainer )
	local childrenPanels = headerContainer.children
	OptionsRestacker.OptionPanels = {}
	for n, childPanel in pairs(childrenPanels) do
		local childControl = childPanel:GetControl()
		local childName = childPanel.data.name:gsub("|c......",""):gsub("|r","")
		if(childControl and childName)then
			OptionsRestacker.OptionPanels[childName] = childControl
		end
		if(n == 1) then 
			_, _, OptionsRestacker.ParentMenuControl = childControl:GetAnchor()
		end
	end	
end

function OptionsRestacker.OR_GetInGameMenuHeaderContainer( menuName )
	local inGameMenu = ZO_GameMenu_InGame
	local headerControls = inGameMenu.gameMenu.headerControls
	local headerContainer = headerControls[menuName]
	if(headerContainer)then
		return headerContainer, headerContainer.childIndent, headerContainer.childSpacing 
	else
		return nil
	end
end

function OptionsRestacker.OR_RemoveMenuHeaderContainerFromSettings( menuName )
	local itemExists = OptionsRestacker.settings.restackMenuHeaders[menuName]
	if(itemExists)then
		OptionsRestacker.settings.restackMenuHeaders[menuName] = nil
	end
end

function OptionsRestacker.OR_GetAllMenuHeaders()
	local inGameMenu = ZO_GameMenu_InGame
	local headerControls = inGameMenu.gameMenu.headerControls 
	if(headerControls)then
		local headerControlNames = {}
		for headerIndex, header in pairs(headerControls) do
			table.insert(headerControlNames, headerIndex)
		end
		return headerControlNames
	else
		return nil
	end
end

function OptionsRestacker.OR_StatusAlert( message )
	ZO_Alert(OptionsRestacker.defaultAlertType, OptionsRestacker.defaultAlertSound, message)
end

--[[--------------------------------------------------------------
--	OptionsRestacker.OptionsRestacker_Loaded
		Receives: eventCode, addOnName
			CallBack for AddOn Initializeation - confirms addOnName, sets default OptionsRestacker.settings, 
			loads saves OptionsRestacker.settings, registers SLASH_COMMANDS to handler callBack function, registers
			for events, etc.
--]]--------------------------------------------------------------
function OptionsRestacker.OptionsRestacker_Loaded(eventCode, addOnName)
	if(addOnName ~= "OptionsRestacker") then
        return
    end	
	
	local defaultSettings = {
		removeColorFromSettings = true,
		ignoreDefaultSettingsPanels = true,
		restackAlphabetically = true,
		restackCustomOrder = {},
		restackMenuHeaders = {
			[OptionsRestacker.SI_GAME_MENU_SETTINGS] = false,
			[OptionsRestacker.SI_GAME_MENU_CONTROLS] = false
		}
	}
	
	OptionsRestacker.settings = ZO_SavedVars:NewAccountWide("OptionsRestacker_Settings", 1, "Settings", defaultSettings)
	
    SLASH_COMMANDS["restacker"] = OptionsRestacker.OR_SlashCommands;

    SCENE_MANAGER.scenes.gameMenuInGame:RegisterCallback("StateChange", 
    	function(...)
			OptionsRestacker.OR_DelayedGameMenu(SCENE_MANAGER.scenes.gameMenuInGame, ...)
		end)

    OptionsRestacker.OR_CreateSettingsWindow()
    OptionsRestacker.OR_SetupORSettingsBackpack( true, OptionsRestacker.lastPanelControl )
end

function OptionsRestacker.OR_CreateSettingsWindow()

	if( LAM_optionsRestacker == nil) then
		LAM_optionsRestacker = LAM:CreateControlPanel("_optionsRestacker", "Options Restacker")
	end

	LAM:AddHeader(LAM_optionsRestacker, "_optionsRestacker_ORHeader", "Options Restacker")

	LAM:AddDescription(LAM_optionsRestacker, "_orSortingHeader", "NOTE: All changes will take affect next time the menu is opened.", "Sorting Options")

	LAM:AddCheckbox(LAM_optionsRestacker, "_orSortingAlphabetically", 'Sort Alphabetically', 'Enables/Disables sorting of selected menus alphabetically.',
    function()
      return OptionsRestacker.settings.restackAlphabetically;
    end,
    function()
        if OptionsRestacker.settings.restackAlphabetically then
    		OptionsRestacker.settings.restackAlphabetically = false;
    		OptionsRestacker.OR_StatusAlert("[OptionsRestacker]:RestackAlphabetically[disabled]")	
    	else
		   	OptionsRestacker.settings.restackAlphabetically = true;
    		OptionsRestacker.OR_StatusAlert("[OptionsRestacker]:RestackAlphabetically[enabled]")
    	end
    end,
    true,
    "Custom sort will take precidence if enabled for a menu item EVEN if alphabetical sort is enabled."
	)

	LAM:AddCheckbox(LAM_optionsRestacker, "_orIgnoreDefaultSettings", 'Ignore Default \'Settings\' Menu Items', 'Leaves the items in the \'Settings\' menu unsorted',
    function()
      return OptionsRestacker.settings.ignoreDefaultSettingsPanels;
    end,
    function()
        if OptionsRestacker.settings.ignoreDefaultSettingsPanels then
    		OptionsRestacker.settings.ignoreDefaultSettingsPanels = false;
    		OptionsRestacker.OR_StatusAlert("[OptionsRestacker]:IgnoreDefaultSettingsPanels[disabled]")	
    	else
		   	OptionsRestacker.settings.ignoreDefaultSettingsPanels = true;
    		OptionsRestacker.OR_StatusAlert("[OptionsRestacker]:IgnoreDefaultSettingsPanels[enabled]")
    	end
    end,
    true,
    "Custom sorted items and alphabetically sorted items will follow the default items if this option is enalbed."
	)

	LAM:AddCheckbox(LAM_optionsRestacker, "_orClearTitleColors", 'Clear Title Colors', 'Enables/Disables removing color from titles.',
    function()
      return OptionsRestacker.settings.removeColorFromSettings;
    end,
    function()
        if OptionsRestacker.settings.removeColorFromSettings then
    		OptionsRestacker.settings.removeColorFromSettings = false;
    		OptionsRestacker.OR_StatusAlert("[OptionsRestacker]:RemoveColorFromSettings[disabled]")	
    	else
		   	OptionsRestacker.settings.removeColorFromSettings = true;
    		OptionsRestacker.OR_StatusAlert("[OptionsRestacker]:RemoveColorFromSettings[enabled]")
    	end
    end
	)

	OptionsRestacker.lastPanelControl = _orClearTitleColors
end

function OptionsRestacker.OR_SlashCommands( cmd )
	
	if ( cmd == "" ) then
    	d("[OptionsRestacker]:Very limited right now! More to come in later versions!")
    	d(" ")
    	d("[OptionsRestacker]:Usage - ")
    	d("	/OptionsRestacker [options]")
    	d(" 	")
    	d("	Options")
    	d("		color - Toggles the option to remove color from settings names in the menu.")
		return
	end
	
	if ( cmd == "color" ) then
		if( OptionsRestacker.settings.removeColorFromSettings ) then
			OptionsRestacker.OR_StatusAlert("[OptionsRestacker]:RemoveColorFromSettings[Off]")
			InventoryInsight.settings.removeColorFromSettings = false;
		else
			OptionsRestacker.OR_StatusAlert("[OptionsRestacker]:RemoveColorFromSettings[On]")
			OptionsRestacker.settings.removeColorFromSettings = true;			
		end
		return
	end
end


--[[--------------------------------------------------------------
--	OptionsRestacker.OptionsRestacker_Initialized
		Initializes the addon and registers for the EVENT_ADD_ON_LOADED with a callback to 
		the local function OptionsRestacker.OptionsRestacker_Loaded().
--]]--------------------------------------------------------------
function OptionsRestacker.OptionsRestacker_Initialized()
	EVENT_MANAGER:RegisterForEvent("OptionsRestackerLoaded", EVENT_ADD_ON_LOADED, OptionsRestacker.OptionsRestacker_Loaded)
end


OptionsRestacker.OptionsRestacker_Initialized()