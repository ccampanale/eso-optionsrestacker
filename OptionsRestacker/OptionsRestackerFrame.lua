 OptionsRestacker.selectedMenuHeader = nil


 function OptionsRestacker.OR_UpdateScrollDataLinesData()
    local ORscroll = OR_SettingsFrameScroll
    local index = 0
    ORscroll.DataLines = {}
    local headerContainer = OptionsRestacker.OR_GetInGameMenuHeaderContainer( OptionsRestacker.selectedMenuHeader )
    OptionsRestacker.OR_CollectPanelChildrenFromContainer( headerContainer )
    if(OptionsRestacker.selectedMenuHeader and OptionsRestacker.OptionPanels) then
        if(OptionsRestacker.OptionPanels) then
            for panelIndex, optionPanel in pairs(OptionsRestacker.OptionPanels) do
                --d(panelIndex)
                if(not OptionsRestacker.OR_CustomOrderContains(panelIndex)) then
                    local tempCustomOrderItem = {
                        settingName = panelIndex,
                        isActiveSetting = true
                    }
                    local selectedMenuHeader = OptionsRestacker.settings.restackCustomOrder[OptionsRestacker.selectedMenuHeader]
                    if(selectedMenuHeader) then
                        table.insert(OptionsRestacker.settings.restackCustomOrder[OptionsRestacker.selectedMenuHeader], tempCustomOrderItem)
                    else
                        OptionsRestacker.settings.restackCustomOrder[OptionsRestacker.selectedMenuHeader] = {}
                        table.insert(OptionsRestacker.settings.restackCustomOrder[OptionsRestacker.selectedMenuHeader], tempCustomOrderItem)
                    end
                end
            end
        end
    end
    ORscroll.DataLines = OptionsRestacker.settings.restackCustomOrder[OptionsRestacker.selectedMenuHeader]
 end

 function OptionsRestacker.OR_CustomOrderContains( panelName )
    if(OptionsRestacker.settings.restackCustomOrder[OptionsRestacker.selectedMenuHeader]) then
        for panelIndex, optionPanel in pairs(OptionsRestacker.settings.restackCustomOrder[OptionsRestacker.selectedMenuHeader]) do
            if(optionPanel.settingName == panelName) then
                return true
            end
        end
        return false
    else
        return false
    end
 end

-- TODO: Add areas to pull data from dataLines for settings and adjust IsActiveIcon acordingly
function OptionsRestacker.OR_UpdateORSettingsScroll(...)
    local ORscroll = OR_SettingsFrameScroll
    local index = 0
    ------------------------------------------------------
    ORscroll.DataOffset = ORscroll.DataOffset or 0
    if ORscroll.DataOffset < 0 then ORscroll.DataOffset = 0 end
    if #ORscroll.DataLines == 0 then 
        for i = 1,ORscroll.MaxLines do
            local curLine = ORscroll.Lines[i]
            if( i == 1) then      
                curLine.IsActiveIcon:SetAlpha(0)
                curLine.MoveUpIcon:SetAlpha(0)
                curLine.MoveDownIcon:SetAlpha(0)                          
                curLine.Text:SetText("")  
            else     
                curLine.IsActiveIcon:SetAlpha(0)
                curLine.MoveUpIcon:SetAlpha(0)
                curLine.MoveDownIcon:SetAlpha(0)
                curLine.Text:SetText("")        
            end
        end         
        return 
    end
    ORscroll.Slider:SetMinMax(0,#ORscroll.DataLines - ORscroll.MaxLines)
    for i = 1,ORscroll.MaxLines do
        local curLine = ORscroll.Lines[i]
        local curData = ORscroll.DataLines[ORscroll.DataOffset + i]
        if( curData ~= nil) then
            curLine.isActiveSetting = curData.isActiveSetting
            OptionsRestacker.OR_UpdateIcon(curLine)
            curLine.IsActiveIcon:SetAlpha(1)
            if(i == 1) then
                curLine.MoveUpIcon:SetAlpha(0)
            else
                curLine.MoveUpIcon:SetAlpha(1)
            end
            if(i == 10) then
                curLine.MoveDownIcon:SetAlpha(0)
            else
                curLine.MoveDownIcon:SetAlpha(1)
            end
            curLine.Text:SetText(curData.settingName)  
        else
            curLine.IsActiveIcon:SetAlpha(0)
            curLine.MoveUpIcon:SetAlpha(0)
            curLine.MoveDownIcon:SetAlpha(0)
            curLine.Text:SetText("")        
        end
    end 
end

function OptionsRestacker.OR_UpdateIcon(control)
    if(control.isActiveSetting) then
        control.IsActiveIcon:SetTexture("/esoui/art/buttons/checkbox_checked.dds")
    else
        control.IsActiveIcon:SetTexture("/esoui/art/buttons/checkbox_disabled.dds")
    end
end

function OptionsRestacker.OR_CreateORSettingsScroll(controlName, owner, parent)
    local ORscroll = WINDOW_MANAGER:CreateTopLevelWindow(controlName)    
 
    ORscroll.DataOffset = 0
    ORscroll.MaxLines = 10
    ORscroll.DataLines = {}
    ORscroll.Lines = {}
 
    ORscroll:SetParent(owner)
    ORscroll:SetHeight(owner:GetHeight()-40)
    ORscroll:SetWidth(owner:GetWidth()-20)
    ORscroll:SetAnchor(TOP,parent,BOTTOM, -3, -10)
    ORscroll:SetDrawLayer(DL_BACKGROUND)
    ORscroll:SetMouseEnabled(true)
    ORscroll:SetMovable(false)
    ORscroll:SetClampedToScreen(true)
    ORscroll:SetHidden(false)
    --ORscroll:SetInheritAlpha(false)
    ORscroll:SetHandler("OnMouseWheel",function(self,delta)
        local value = ORscroll.DataOffset - delta
        if value < 0 then 
            value = 0
        elseif value > #ORscroll.DataLines - ORscroll.MaxLines then 
            value = #ORscroll.DataLines - ORscroll.MaxLines 
        end
        ORscroll.DataOffset = value
        ORscroll.Slider:SetValue(ORscroll.DataOffset)
        OptionsRestacker.OR_UpdateORSettingsScroll()
    end)
    ORscroll:SetHandler("OnShow",function(self)
        --OptionsRestacker.OR_UpdateScrollDataLinesData()
        --OptionsRestacker.OR_UpdateORSettingsScroll()
    end)
 
    local tex = "/esoui/art/miscellaneous/scrollbox_elevator.dds"
    ORscroll.Slider = WINDOW_MANAGER:CreateControl(nil,ORscroll,CT_SLIDER)
    ORscroll.Slider:SetDimensions(16,ORscroll:GetHeight())
    ORscroll.Slider:SetMouseEnabled(true)
    ORscroll.Slider:SetThumbTexture(tex,tex,tex,16,50,0,0,1,1)
    ORscroll.Slider:SetValue(0)
    ORscroll.Slider:SetValueStep(1)
    ORscroll.Slider:SetAnchor(LEFT,ORscroll,RIGHT,-5,0)
 
        -- When we change the slider's value we need to change the data offset and redraw the display
    ORscroll.Slider:SetHandler("OnValueChanged",function(self,value,eventReason)
        ORscroll.DataOffset = math.min(value,#ORscroll.DataLines - ORscroll.MaxLines)
        OptionsRestacker.OR_UpdateORSettingsScroll()
    end)
 
    for i=1,ORscroll.MaxLines do
        ORscroll.Lines[i] = WINDOW_MANAGER:CreateControl(ORscroll:GetName().."Line"..i,ORscroll,CT_CONTROL)
        ORscroll.Lines[i]:SetDimensions(ORscroll:GetWidth(),40)
        if i == 1 then
            ORscroll.Lines[i]:SetAnchor(TOPLEFT,ORscroll,TOPLEFT,5,5)
            ORscroll.Lines[i]:SetAnchor(TOPRIGHT,ORscroll,TOPRIGHT,0,5)
        else
            ORscroll.Lines[i]:SetAnchor(TOPLEFT,ORscroll.Lines[i-1],BOTTOMLEFT,0,0)
            ORscroll.Lines[i]:SetAnchor(TOPRIGHT,ORscroll.Lines[i-1],BOTTOMRIGHT,0,0)
        end

        ORscroll.Lines[i].isActiveSetting = true -- represents the active state of the settings title the line represents

        ORscroll.Lines[i].bg = WINDOW_MANAGER:CreateControl(ORscroll.Lines[i]:GetName().."BG", ORscroll.Lines[i], CT_TEXTURE)
        local bg = ORscroll.Lines[i].bg
        bg:SetTexture("/esoui/art/actionbar/classbar_bg.dds")        
        bg:SetTextureCoords(0,1,0,1)
        bg:SetAlpha(1)
        bg:SetAnchor(TOPLEFT, ORscroll.Lines[i], TOPLEFT, -10, 0)
        bg:SetAnchor(BOTTOMRIGHT, ORscroll.Lines[i], BOTTOMRIGHT, 10, 5)


        ORscroll.Lines[i].IsActiveIcon = WINDOW_MANAGER:CreateControl(ORscroll.Lines[i]:GetName().."IsActiveIcon",ORscroll.Lines[i],CT_TEXTURE)
        ORscroll.Lines[i].IsActiveIcon:SetDimensions(20,20)
        ORscroll.Lines[i].IsActiveIcon:SetAnchor(RIGHT,ORscroll.Lines[i],RIGHT,-5,0)
        ORscroll.Lines[i].IsActiveIcon:SetTexture("/esoui/art/buttons/checkbox_indeterminate.dds")
        ORscroll.Lines[i].IsActiveIcon:SetTextureCoords(0,1,0,1)
        ORscroll.Lines[i].IsActiveIcon:SetAlpha(0)    
        ORscroll.Lines[i].IsActiveIcon:SetMouseEnabled(true)
        
        ORscroll.Lines[i].IsActiveIcon:SetHandler("OnMouseEnter",
            function(self)
                if(self:GetParent().Text:GetText() ~= "Options Restacker")then
                    self:SetTexture("/esoui/art/buttons/checkbox_indeterminate.dds")
                end
            end)
        
        ORscroll.Lines[i].IsActiveIcon:SetHandler("OnMouseExit",
            function(self)
                OptionsRestacker.OR_UpdateIcon(ORscroll.Lines[i])
            end)
        ORscroll.Lines[i].IsActiveIcon:SetHandler("OnMouseUp",
            function(self)
                if(self:GetParent().Text:GetText() ~= "Options Restacker")then
                    for customItemIndex, customItem in pairs(OptionsRestacker.settings.restackCustomOrder[OptionsRestacker.selectedMenuHeader]) do
                        --d(customItem.settingName..":"..self:GetParent().Text:GetText())
                        if(customItem.settingName == self:GetParent().Text:GetText())then
                            self:GetParent().isActiveSetting = not self:GetParent().isActiveSetting
                            OptionsRestacker.settings.restackCustomOrder[OptionsRestacker.selectedMenuHeader][customItemIndex].isActiveSetting = not OptionsRestacker.settings.restackCustomOrder[OptionsRestacker.selectedMenuHeader][customItemIndex].isActiveSetting
                            return
                        end
                    end
                    OptionsRestacker.OR_UpdateIcon(ORscroll.Lines[i])
                end
            end)

        ORscroll.Lines[i].MoveUpIcon = WINDOW_MANAGER:CreateControl(ORscroll.Lines[i]:GetName().."MoveUpIcon",ORscroll.Lines[i],CT_TEXTURE)
        ORscroll.Lines[i].MoveUpIcon:SetDimensions(20,20)
        ORscroll.Lines[i].MoveUpIcon:SetAnchor(RIGHT,ORscroll.Lines[i].IsActiveIcon,TOPLEFT,-5,0)
        ORscroll.Lines[i].MoveUpIcon:SetTexture("/esoui/art/buttons/pointsplus_up.dds")
        ORscroll.Lines[i].MoveUpIcon:SetTextureCoords(0,1,0,1)
        ORscroll.Lines[i].MoveUpIcon:SetAlpha(0)
        ORscroll.Lines[i].MoveUpIcon:SetMouseEnabled(true)
        ORscroll.Lines[i].MoveUpIcon:SetHandler("OnMouseEnter",
            function(self)
                self:SetTexture("/esoui/art/buttons/pointsplus_over.dds")
            end)
        ORscroll.Lines[i].MoveUpIcon:SetHandler("OnMouseExit",
            function(self)
                self:SetTexture("/esoui/art/buttons/pointsplus_up.dds")
            end)
        ORscroll.Lines[i].MoveUpIcon:SetHandler("OnMouseUp",
            function(self)
                --move the represented setting up the chain
                for customItemIndex, customItem in pairs(OptionsRestacker.settings.restackCustomOrder[OptionsRestacker.selectedMenuHeader]) do
                    if(customItem.settingName == self:GetParent().Text:GetText())then
                        local tempCur = OptionsRestacker.settings.restackCustomOrder[OptionsRestacker.selectedMenuHeader][customItemIndex]
                        OptionsRestacker.settings.restackCustomOrder[OptionsRestacker.selectedMenuHeader][customItemIndex] = OptionsRestacker.settings.restackCustomOrder[OptionsRestacker.selectedMenuHeader][customItemIndex-1]
                        OptionsRestacker.settings.restackCustomOrder[OptionsRestacker.selectedMenuHeader][customItemIndex-1] = tempCur
                        OptionsRestacker.OR_UpdateORSettingsScroll()
                        --OptionsRestacker.OR_SetNewOptionsPanelOrder()
                        return
                    end
                end
            end)

        ORscroll.Lines[i].MoveDownIcon = WINDOW_MANAGER:CreateControl(ORscroll.Lines[i]:GetName().."MoveDownIcon",ORscroll.Lines[i],CT_TEXTURE)
        ORscroll.Lines[i].MoveDownIcon:SetDimensions(20,20)
        ORscroll.Lines[i].MoveDownIcon:SetAnchor(RIGHT,ORscroll.Lines[i].IsActiveIcon,BOTTOMLEFT,-5,0)
        ORscroll.Lines[i].MoveDownIcon:SetTexture("/esoui/art/buttons/pointsminus_up.dds")
        ORscroll.Lines[i].MoveDownIcon:SetTextureCoords(0,1,0,1)
        ORscroll.Lines[i].MoveDownIcon:SetAlpha(0)
        ORscroll.Lines[i].MoveDownIcon:SetMouseEnabled(true)
        ORscroll.Lines[i].MoveDownIcon:SetHandler("OnMouseEnter",
            function(self)
                self:SetTexture("/esoui/art/buttons/pointsminus_over.dds")
            end)
        ORscroll.Lines[i].MoveDownIcon:SetHandler("OnMouseExit",
            function(self)
                self:SetTexture("/esoui/art/buttons/pointsminus_up.dds")
            end)
        ORscroll.Lines[i].MoveDownIcon:SetHandler("OnMouseUp",
            function(self)
                --move the represented setting down the chain
                for customItemIndex, customItem in pairs(OptionsRestacker.settings.restackCustomOrder[OptionsRestacker.selectedMenuHeader]) do
                    if(customItem.settingName == self:GetParent().Text:GetText())then
                        local tempCur = OptionsRestacker.settings.restackCustomOrder[OptionsRestacker.selectedMenuHeader][customItemIndex]
                        OptionsRestacker.settings.restackCustomOrder[OptionsRestacker.selectedMenuHeader][customItemIndex] = OptionsRestacker.settings.restackCustomOrder[OptionsRestacker.selectedMenuHeader][customItemIndex+1]
                        OptionsRestacker.settings.restackCustomOrder[OptionsRestacker.selectedMenuHeader][customItemIndex+1] = tempCur
                        OptionsRestacker.OR_UpdateORSettingsScroll()
                        --OptionsRestacker.OR_SetNewOptionsPanelOrder()
                        return
                    end
                end
            end)



 
        ORscroll.Lines[i].Text = WINDOW_MANAGER:CreateControl(ORscroll.Lines[i]:GetName().."Text",ORscroll.Lines[i],CT_LABEL)
        ORscroll.Lines[i].Text:SetFont("ZoFontGame")
        --ORscroll.Lines[i].Text:SetDimensions(ORscroll.Lines[i]:GetWidth()-90,30)
        ORscroll.Lines[i].Text:SetAnchor(LEFT,ORscroll.Lines[i],LEFT,5,0)
        ORscroll.Lines[i].Text:SetAnchor(RIGHT,ORscroll.Lines[i].IsActiveIcon,LEFT,-20,0)
        ORscroll.Lines[i].Text:SetText("")
        

        ORscroll.Lines[i]:SetHidden(false)
        ORscroll.Lines[i]:SetMouseEnabled(true)
        ORscroll.Lines[i]:SetHandler("OnMouseEnter", function(control)
            --Possibly add some sort of InformationTooltip about the setting panel this represents? Check objects and be careful...
        end)
        ORscroll.Lines[i]:SetHandler("OnMouseExit", function(self)
            --see above...
        end)
    end

    OR_SettingsFrameScroll = ORscroll
end

function OptionsRestacker.OR_CreateORSettingsFrame(controlName, owner, parent)
    local frame = WINDOW_MANAGER:CreateControlFromVirtual(controlName, owner, "ZO_DefaultBackdrop")
    frame:ClearAnchors()
    frame:SetAnchor(TOP, parent, BOTTOM, 20, 40) --ZO_SharedThinLeftPanelBackground anchor
    frame:SetDimensions(490, 450)
    frame.controlType = CT_CONTROL
    frame.system = SETTING_TYPE_UI
    frame:SetHidden(false)
    frame:SetMouseEnabled(true)
    frame:SetMovable(false)
    frame:SetClampedToScreen(true)

    frame.label = WINDOW_MANAGER:CreateControl(controlName.."Label", frame, CT_LABEL)
    local label = frame.label
    label:SetAnchor(BOTTOM, frame, TOP, 0, -5)
    label:SetFont("ZoFontWinH4")
    label:SetText("Custom Sort")

    frame.Info = WINDOW_MANAGER:CreateControl(frame:GetName().."Info",frame,CT_TEXTURE)
    frame.Info:SetDimensions(20,20)
    frame.Info:SetAnchor(TOPRIGHT,frame,TOPRIGHT,-5,5)
    frame.Info:SetTexture("/esoui/art/buttons/info_up.dds")
    frame.Info:SetTextureCoords(0,1,0,1)
    frame.Info:SetDrawLayer(0)
    frame.Info:SetDrawLevel(2)
    frame.Info:SetDrawTier(1)  
    frame.Info:SetMouseEnabled(true)
    frame.Info:SetHandler("OnMouseEnter", function(self)
        self:SetTexture("/esoui/art/buttons/info_over.dds")
        InitializeTooltip(InformationTooltip, self, LEFT, 0, 0, 0)
        InformationTooltip:SetHidden(false)
        InformationTooltip:ClearLines()
        InformationTooltip:AddLine("Move items up the list by pressing the plus (+) icon.\nMove items down the list by pressing the minus (-) icon.\nDisable/Enable items in the list showing in the menu with the checkbox.")
    end)
    frame.Info:SetHandler("OnMouseExit", function(self)
        self:SetTexture("/esoui/art/buttons/info_up.dds")
        InformationTooltip:SetHidden(true)
        InformationTooltip:ClearLines()
    end)

    --[[
    frame.Close = WINDOW_MANAGER:CreateControl(nil,frame,CT_BUTTON)
    frame.Close:SetDimensions(30, 30)
    frame.Close:SetAnchor(TOPRIGHT,frame,TOPRIGHT,-2,2)
    frame.Close:SetHidden(false)
    frame.Close:SetFont("ZoFontGameLargeBoldShadow")
    frame.Close:SetText("X")
    frame.Close:SetHandler("OnClicked",function(self) 
        frame:SetHidden(true)    
    end)
    ]]

    return frame
end


function OptionsRestacker.OR_CreateORSettingsDropdown(controlName, text, tooltip, validChoices, getFunc, setFunc, owner, parent)
    local dropdown = WINDOW_MANAGER:CreateControlFromVirtual(controlName, owner, "ZO_Options_Dropdown")
    dropdown:SetAnchor(TOP, parent, TOP, 0, 0)
    dropdown:SetDimensions(190, 50)
    --dropdown:SetInheritAlpha(false)
    dropdown.controlType = OPTIONS_DROPDOWN
    dropdown.system = SETTING_TYPE_UI
    dropdown.text = text
    dropdown.tooltipText = tooltip
    dropdown.valid = validChoices
    local dropmenu = ZO_ComboBox_ObjectFromContainer(GetControl(dropdown, "Dropdown"))
    local setText = dropmenu.m_selectedItemText.SetText
    dropdown.dropmenu = dropmenu
    ZO_PreHookHandler(dropmenu.m_selectedItemText, "OnTextChanged", function(self)
            if dropmenu.m_selectedItemData then
                selectedName = dropmenu.m_selectedItemData.name
                setText(self, selectedName)
                setFunc(selectedName)
            end
        end)
    dropdown:SetHandler("OnShow", function()
            dropmenu:SetSelectedItem(getFunc())
        end)

    local function OnItemSelect(_, choiceText, choice)  --this is the callback function for when an item gets selected in the dropdown
        --d(choiceText, choice)
    end
     
    for i=1,#validChoices do  --loop through your table to add the selections to the dropdown
        local entry = dropmenu:CreateItemEntry(validChoices[i], OnItemSelect)  --this really just creates a table with {name = choices[i], callback = OnItemSelect} - you may be able to skip this step and just pass the correctly formatted table into the below function...
        dropmenu:AddItem(entry)  --again, entry is just a table with the above args stored in it
    end

    dropdown:SetHidden(false)
    dropdown:SetMouseEnabled(true)


    dropdown.IsActiveIcon = WINDOW_MANAGER:CreateControl(dropdown:GetName().."IsActiveIcon",dropdown,CT_TEXTURE)
    dropdown.IsActiveIcon:SetDimensions(20,20)
    dropdown.IsActiveIcon:SetAnchor(LEFT,dropdown,RIGHT,5,0)
    dropdown.IsActiveIcon:SetTexture("/esoui/art/buttons/checkbox_disabled.dds")
    dropdown.IsActiveIcon:SetTextureCoords(0,1,0,1)
    dropdown.IsActiveIcon:SetAlpha(1)    
    dropdown.IsActiveIcon:SetMouseEnabled(true)
    
    dropdown.IsActiveIcon:SetHandler("OnMouseEnter",
        function(self)
            if(OptionsRestacker.selectedMenuHeader) then
                self:SetTexture("/esoui/art/buttons/checkbox_indeterminate.dds")
                InitializeTooltip(InformationTooltip, self, LEFT, 0, 0, 0)
                InformationTooltip:SetHidden(false)
                InformationTooltip:ClearLines()
                InformationTooltip:AddLine("Check the box to enable the custom sort for the items in this portion of the game menu.")
            end
        end)
    
    dropdown.IsActiveIcon:SetHandler("OnMouseExit",
        function(self)
            -- check selected
            if(OptionsRestacker.selectedMenuHeader) then
                if(OptionsRestacker.settings.restackCustomOrder[OptionsRestacker.selectedMenuHeader] and OptionsRestacker.settings.restackMenuHeaders[OptionsRestacker.selectedMenuHeader]) then
                    self:SetTexture("/esoui/art/buttons/checkbox_checked.dds")
                else
                    self:SetTexture("/esoui/art/buttons/checkbox_disabled.dds")
                end
            end
            InformationTooltip:SetHidden(true)
            InformationTooltip:ClearLines()
        end)
    dropdown.IsActiveIcon:SetHandler("OnMouseUp",
        function(self)
            if(OptionsRestacker.selectedMenuHeader) then
                OptionsRestacker.settings.restackMenuHeaders[OptionsRestacker.selectedMenuHeader] = not OptionsRestacker.settings.restackMenuHeaders[OptionsRestacker.selectedMenuHeader]
                if(OptionsRestacker.settings.restackCustomOrder[OptionsRestacker.selectedMenuHeader] and OptionsRestacker.settings.restackMenuHeaders[OptionsRestacker.selectedMenuHeader]) then
                    self:SetTexture("/esoui/art/buttons/checkbox_checked.dds")
                else
                    self:SetTexture("/esoui/art/buttons/checkbox_disabled.dds")
                end
            end
        end)

    return dropdown
end

function OptionsRestacker.OR_DropmenuRefreshItems()
    OR_SettingsDropdownControl.dropmenu:ClearItems()
    local validChoices = OptionsRestacker.OR_GetDropdownMenuOptions()
    for i=1,#validChoices do  --loop through your table to add the selections to the dropdown
        local entry = OR_SettingsDropdownControl.dropmenu:CreateItemEntry(validChoices[i], OnItemSelect)  --this really just creates a table with {name = choices[i], callback = OnItemSelect} - you may be able to skip this step and just pass the correctly formatted table into the below function...
        OR_SettingsDropdownControl.dropmenu:AddItem(entry)  --again, entry is just a table with the above args stored in it
    end
end

function OptionsRestacker.OR_SetupORSettingsBackpack( setup, parent )
    if( setup ) then
        if(OR_SettingsFrame == nil) then
            OR_SettingsFrame = OptionsRestacker.OR_CreateORSettingsFrame("OR_SettingsFrame", parent, parent)
        end
        if(OR_SettingsDropdownControl == nil) then
            local gameMenuHeaders = OptionsRestacker.OR_GetDropdownMenuOptions();
            OR_SettingsDropdownControl = OptionsRestacker.OR_CreateORSettingsDropdown(
                "OR_SettingsDropdownControl",
                "OR_SettingsDropdownControl", 
                "Select menu header to edit.",
                gameMenuHeaders,
                OptionsRestacker.OR_GetSelectedHeader,
                OptionsRestacker.OR_SetSelectedHeader,
                OR_SettingsFrame,
                OR_SettingsFrame
            )
        end
        if(OR_SettingsFrameScroll == nil) then
            OptionsRestacker.OR_CreateORSettingsScroll("OR_SettingsFrameScroll", OR_SettingsFrame, OR_SettingsDropdownControl)      
        end
    end
end


function OptionsRestacker.OR_GetDropdownMenuOptions()
    --local gameMenuHeaders = OptionsRestacker.OR_GetAllMenuHeaders()
    local gameMenuHeaders = { OptionsRestacker.SI_GAME_MENU_SETTINGS, OptionsRestacker.SI_GAME_MENU_CONTROLS}
    if(gameMenuHeaders) then
        return gameMenuHeaders
    end
    return nil
end


function OptionsRestacker.OR_SetSelectedHeader( newHeader )
    OptionsRestacker.selectedMenuHeader = newHeader
    if(OptionsRestacker.selectedMenuHeader) then
        if(OptionsRestacker.settings.restackCustomOrder[OptionsRestacker.selectedMenuHeader] and OptionsRestacker.settings.restackMenuHeaders[OptionsRestacker.selectedMenuHeader]) then
            OR_SettingsDropdownControl.IsActiveIcon:SetTexture("/esoui/art/buttons/checkbox_checked.dds")
        else
            OR_SettingsDropdownControl.IsActiveIcon:SetTexture("/esoui/art/buttons/checkbox_disabled.dds")
        end
    end    
    OptionsRestacker.OR_UpdateScrollDataLinesData()
    OptionsRestacker.OR_UpdateORSettingsScroll()
end


function OptionsRestacker.OR_GetSelectedHeader()
    return  OptionsRestacker.selectedMenuHeader
end

